package ogame;


import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class OgameSimpleBot
        implements Runnable {
    // Selenium stuff
    WebDriver driver;
    JavascriptExecutor js;

    //Logger

    boolean metalMineImproveButtonIsExist;
    boolean crystalMineImproveButtonIsExist;
    boolean deuteriumMineImproveButtonIsExist;
    boolean energyMineImproveButtonIsExist;
    boolean metalStorageImproveButtonIsExist;
    boolean crystalStorageImproveButtonIsExist;
    boolean deuteriumStorageImproveButtonIsExist;
    boolean constructionQueueIsActive;

    // Config
    String siteName = "https://ru.ogame.gameforge.com/#login";

    String ogameLogin = "2dl@tut.by";
    String ogamePassword = "111222";

    Integer metalAvailable, crystalAvailable, deuteriumAvailable, energyAvailable;

    String metalMineImproveButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[1]/div/div/a[1]/img";
    String crystalMineUpdateButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[2]/div/div/a[1]/img";
    String deuteriumMineImproveButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[3]/div/div/a[1]/img";
    String energyMineImproveButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[4]/div/div/a[1]/img";
    String metalStorageImproveButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[1]/div/div/a[2]/img";
    String crystalStorageImproveButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[2]/div/div/a[2]/img";
    String deuteriumStorageImproveButtonIsExistXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[3]/div/div/a[2]/img";
    String playGameButtonXPath = "/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/a/button/span";
    String playGameButton2Xpath = "/html/body/div[1]/div/div/div[1]/div[2]/section[1]/div[2]/div/div/div[1]/div[2]/div/div/div[11]/button";
    String gameLoginXPath = "/html/body/div[1]/div/div/div/div[2]/ul/li[1]/span";
    String submitButtonXPath = "/html/body/div[1]/div/div/div/div[2]/div/form/p/button[1]/span";
    String metalAvailableXPath = "//*[@id=\"resources_metal\"]";
    String crystalAvailableXPath = "//*[@id=\"resources_crystal\"]";
    String deuteriumAvailableXPath = "//*[@id=\"resources_deuterium\"]";
    String energyAvailableXPath = "//*[@id=\"resources_energy\"]";
    String resourcesXPath = "/html/body/div[2]/div[2]/div/div[2]/div/ul/li[2]/a/span";
    String metalStorageImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[1]/div/div/a[2]/img";
    String crystalStorageImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[2]/div/div/a[2]/img";
    String deuteriumStorageImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[3]/div/div/a[2]/img";
    String energyMineImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[4]/div/div/a[1]/img";
    String metalMineImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[1]/div/div/a[1]/img";
    String crystalMineImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[2]/div/div/a[1]/img";
    String deuteriumMineImproveButtonXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[3]/div/div/a[1]/img";
    String constructionQueue = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[5]/div[2]/table[2]/tbody/tr/td[1]/a/img";

    String merchantXPath = "/html/body/div[2]/div[2]/div/div[2]/div/ul/li[4]/a/span";
    String auctioneerXPath = "//*[@id=\"js_traderAuctioneer\"]";
    String nextAuctionInXPath = "//*[@id=\"nextAuction\"]";
    String auctionFinishAtXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/div[1]/div[2]/p/span/b";

    String homeWorldXPath = "/html/body/div[2]/div[2]/div/div[4]/div/div/div[2]/div[1]/a[1]/img";
    String colonyBeerXPath = "/html/body/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/a/img";
    String colonyVodkaXPath = "/html/body/div[2]/div[2]/div/div[4]/div/div/div[2]/div[3]/a/span[1]";
    String currentBetXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/div[1]/div[2]/div[3]";
    String currentPlayerXPath = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/div[1]/div[2]/a";
    String betCSSSelector = ".js_sliderMetalMax"; //.js_sliderDeuteriumMax
    String betPushClassName = "pay";

    public void gameLogin(WebDriverWait wait) {
        WebElement login = driver.findElement(By.xpath(gameLoginXPath));
        login.click();
        WebElement formEmail = driver.findElement(By.name("email"));
        formEmail.sendKeys(ogameLogin);
        WebElement formPassword = driver.findElement(By.name("password"));
        formPassword.sendKeys(ogamePassword);
        WebElement submitButton = driver.findElement(By.xpath(submitButtonXPath));
        submitButton.click();
        if (driver.getTitle().equals("OGame Главная")) {
            //  System.out.println("===[Logined - ok!]===");
        }
    }

    public void joinToGame(WebDriverWait wait) {
        WebElement playGameButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(playGameButtonXPath)));
        playGameButton.click();
        playGameButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(playGameButton2Xpath)));
        playGameButton.click();

        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());

        driver.switchTo().window(tabs.get(0));
        driver.close();
        driver.switchTo().window(tabs.get(1));
    }

    public void getResourceValues(WebDriverWait wait) {
        WebElement metalRaw = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(metalAvailableXPath)));
        metalAvailable = Integer.parseInt(metalRaw.getText().replaceAll("[.]", ""));

        WebElement crystalRaw = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(crystalAvailableXPath)));
        crystalAvailable = Integer.parseInt(crystalRaw.getText().replaceAll("[.]", ""));

        WebElement deuteriumRaw = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(deuteriumAvailableXPath)));
        deuteriumAvailable = Integer.parseInt(deuteriumRaw.getText().replaceAll("[.]", ""));

        WebElement energyRaw = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(energyAvailableXPath)));
        energyAvailable = Integer.parseInt(energyRaw.getText().replaceAll("[.]", ""));
        /*System.out.println("================");
        System.out.println("Metal = " + metalAvailable);
        System.out.println("Crystal = " + crystalAvailable);
        System.out.println("Deuterium = " + deuteriumAvailable);
        System.out.println("Energy = " + energyAvailable);
        System.out.println("================");*/
    }

    public void openResourcesLink(WebDriverWait wait) {
        WebElement mines = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(resourcesXPath)));
        mines.click();
    }

    public void colonyBeerOpen(WebDriverWait wait) {
        WebElement link = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(colonyBeerXPath)));
        link.click();
    }

    public void homeWorldOpen(WebDriverWait wait) {
        WebElement link = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(homeWorldXPath)));
        link.click();
    }

    public void colonyVodkaOpen(WebDriverWait wait) {
        WebElement link = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(colonyVodkaXPath)));
        link.click();
    }

    public void storageImprove(WebDriverWait wait) {
        buttonsExistUpdate();
        WebElement metalBoxColor = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(metalAvailableXPath)));
        if ((metalBoxColor.getAttribute("class").equals("overmark") && (metalStorageImproveButtonIsExist)) && (!constructionQueueIsActive)) {
            WebElement metalBoxUpdate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(metalStorageImproveButtonXPath)));
            metalBoxUpdate.click();
            //System.out.println("MetalBoxUpdate>");
        }

        buttonsExistUpdate();
        WebElement crystalBoxColor = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(crystalAvailableXPath)));
        if ((crystalBoxColor.getAttribute("class").equals("overmark") && (crystalStorageImproveButtonIsExist)) && (!constructionQueueIsActive)) {
            WebElement crystalBoxUpdate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(crystalStorageImproveButtonXPath)));
            crystalBoxUpdate.click();
            //System.out.println("CrystalBoxUpdate>");
        }

        buttonsExistUpdate();
        WebElement deuteriumBoxColor = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(deuteriumAvailableXPath)));
        if ((deuteriumBoxColor.getAttribute("class").equals("overmark") && (deuteriumStorageImproveButtonIsExist)) && (!constructionQueueIsActive)) {
            WebElement deuteriumBoxUpdate = wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(deuteriumStorageImproveButtonXPath)));
            deuteriumBoxUpdate.click();
            //System.out.println("DeuteriumBoxUpdate>");
        }
    }

    public void minesUpdate(WebDriverWait wait) {
        buttonsExistUpdate();
        if ((energyAvailable < 0) && (energyMineImproveButtonIsExist) && (!constructionQueueIsActive)) {
            WebElement energyMineUpdateButton = wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(energyMineImproveButtonXPath)));
            energyMineUpdateButton.click();
            //System.out.println("EnergyMineUpdate>");
        }

        buttonsExistUpdate();
        if ((energyAvailable > 0) && (metalMineImproveButtonIsExist) && (!constructionQueueIsActive)) {
            WebElement metalMineUpdateButton = wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(metalMineImproveButtonXPath)));
            metalMineUpdateButton.click();
            //System.out.println("MetalMineUpdate>");
        }

        buttonsExistUpdate();
        if ((energyAvailable > 0) && (crystalMineImproveButtonIsExist) && (!constructionQueueIsActive)) {
            WebElement crystalMineUpdateButton = wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(crystalMineImproveButtonXPath)));
            crystalMineUpdateButton.click();
            //System.out.println("CrystalMineUpdate>");
        }

        buttonsExistUpdate();
        if ((energyAvailable > 0) && (deuteriumMineImproveButtonIsExist) && (!constructionQueueIsActive)) {
            WebElement deuteriumMineUpdateButton = wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(deuteriumMineImproveButtonXPath)));
            deuteriumMineUpdateButton.click();
            // System.out.println("DeuteriumMineUpdate>");
        }
    }

    public boolean isElementExists(By xpath) {
        try {
            driver.findElement(xpath);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }



    public boolean waitForElement(By xpath, int timeInSeconds) {
        try {
            for (int i = 0; i < timeInSeconds; i++) {
                if (isElementExists(xpath))
                    return true;
                Thread.sleep(1000);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public void auctioneer(WebDriverWait wait) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        String minutes = sdf.format(cal.getTime());
        System.out.println(minutes);

    }

    public void buttonsExistUpdate() {
        metalMineImproveButtonIsExist = isElementExists(By.xpath(metalMineImproveButtonIsExistXPath));
        crystalMineImproveButtonIsExist = isElementExists(By.xpath(crystalMineUpdateButtonIsExistXPath));
        deuteriumMineImproveButtonIsExist = isElementExists(By.xpath(deuteriumMineImproveButtonIsExistXPath));
        energyMineImproveButtonIsExist = isElementExists(By.xpath(energyMineImproveButtonIsExistXPath));
        metalStorageImproveButtonIsExist = isElementExists(By.xpath(metalStorageImproveButtonIsExistXPath));
        crystalStorageImproveButtonIsExist = isElementExists(By.xpath(crystalStorageImproveButtonIsExistXPath));
        deuteriumStorageImproveButtonIsExist = isElementExists(By.xpath(deuteriumStorageImproveButtonIsExistXPath));
        constructionQueueIsActive = isElementExists(By.xpath(constructionQueue));
    }

    public void auctioneerStatusCheck(WebDriverWait wait) {
        WebElement traderPage = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(merchantXPath)));
        traderPage.click();
        traderPage = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(auctioneerXPath)));
        traderPage.click();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String timeDate = sdf.format(cal.getTime());

        if (waitForElement(By.xpath(nextAuctionInXPath), 3)) {
            WebElement traderTimer = wait.until((ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(nextAuctionInXPath))));
            System.out.println(timeDate + " Next in " + traderTimer.getText());
        }

        if (waitForElement(By.xpath(auctionFinishAtXPath), 3)) {
            WebElement traderTimer = wait.until((ExpectedConditions.visibilityOfElementLocated(
                    By.xpath(auctionFinishAtXPath))));
            System.out.println(timeDate + " Finish in " + traderTimer.getText());
            WebElement currentBet = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(currentBetXPath)));
            int currentBetInt = Integer.parseInt(currentBet.getText().replaceAll("[.]", ""));
            System.out.println("Current Bet = " + currentBetInt);

            if (isElementExists(By.xpath(currentPlayerXPath))) {
                WebElement currentBuyer = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(currentPlayerXPath)));
                System.out.println("Current buyer = " + currentBuyer.getText());
                System.out.println("Current player != brd " + !currentBuyer.getText().equals("brd"));
                System.out.println("Can click bet? " + isElementExists(By.cssSelector(betCSSSelector)));
                System.out.println("Time <5 min? " + traderTimer.getText().equals("ок. 5м"));
                if ((!currentBuyer.getText().equals("brd")) && (isElementExists(By.cssSelector(betCSSSelector))) && traderTimer.getText().equals("ок. 5м")
                ) {
                    WebElement bet = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(betCSSSelector)));
                    System.out.println("Can bet");
                    bet.click();
                    if (isElementExists(By.className(betPushClassName))) {
                        System.out.println("Can push!");
                        WebElement betPush = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(betPushClassName)));
                        if (currentBetInt < 25000) {
                            System.out.println("Current Bet < 25000? " + (currentBetInt < 25000));
                            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", betPush);
                            betPush.click();
                            System.out.println("Push Bet!");
                        }
                    }
                }
            }
        }traderPage = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(merchantXPath)));
    }

    public void run() {
        try {
//            System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\selenium\\geckodriver.exe");
            System.setProperty("webdriver.gecko.driver", "src/main/java/recource/geckodriver.exe");

//            File file = new File("src/main/java/recource/uBlock0_1.24.3b1.firefox.signed.xpi");
            //final String firebugPath = "C:\\adblock_plus-3.7-an+fx.xpi";
            //FirefoxProfile fp = new FirefoxProfile();
            //fp.addExtension(new File(firebugPath));
            driver = new FirefoxDriver();
            WebDriverWait wait = new WebDriverWait(driver, 100);


            driver.get(siteName);
            driver.manage().window().maximize();
            //river.manage().window().setPosition(new Point(0,-1000));
            gameLogin(wait);

            joinToGame(wait);

            int i = 0;
            while (i < 10) {
                i++;
                //System.out.println("I = " + i);
                homeWorldOpen(wait);
                getResourceValues(wait);
                openResourcesLink(wait);
                storageImprove(wait);
                minesUpdate(wait);

                //auctioneerStatusCheck(wait);

                colonyBeerOpen(wait);
                getResourceValues(wait);
                openResourcesLink(wait);
                storageImprove(wait);
                minesUpdate(wait);

                colonyVodkaOpen(wait);
                getResourceValues(wait);
                openResourcesLink(wait);
                storageImprove(wait);
                minesUpdate(wait);

                //auctioneer(wait);

                sleep(20000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // sleep(10000);
            driver.quit();
        }
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        while (true) {
            (new OgameSimpleBot()).run();
            sleep(5000);
        }
    }
}
