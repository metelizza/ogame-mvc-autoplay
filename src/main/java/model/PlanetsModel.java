package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PlanetsModel extends ModelBase {

    public PlanetsModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"planet-33629740\"]/a")
    WebElement homeWorld;

    @FindBy(how = How.XPATH, using = "//*[@id=\"planet-33630923\"]/a[1]")
    WebElement colonyBeer;

    @FindBy(how = How.XPATH, using = "//*[@id=\"planet-33631905\"]/a")
    WebElement colonyVodka;

    @FindBy(how = How.XPATH, using = "//*[@id=\"planet-33633232\"]/a[1]")
    WebElement colonyGin;



    @FindBy(how = How.XPATH, using = "//*[@id=\"planet-33635327\"]/a")
    WebElement colonyWine;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[4]/div/div/div[2]/div[6]/a[1]/span[1]")
    WebElement colonySidre;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[4]/div/div/div[2]/div[7]/a/span[1]")
    WebElement calvados;

    @FindBy (how = How.XPATH, using = "//*[@id=\"planet-33629740\"]/a[2]/img")
    WebElement moonHome;


    public WebElement colonyCalvadosButton() {
        return calvados;
    }

    public WebElement colonySidreButton() {
        return colonySidre;
    }

    public WebElement colonyWineButton() {return colonyWine;}

    public WebElement homeWorldButton() {
        return homeWorld;
    }

    public WebElement colonyBeerButton() {
        return colonyBeer;
    }

    public WebElement colonyVodkaButton() {
        return colonyVodka;
    }

    public WebElement getColonyGin() {
        return colonyGin;
    }

    public WebElement getMoonHome() {
        return moonHome;
    }
}

