package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class FabriqeModel extends ModelBase {
    public FabriqeModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"menuTable\"]/li[3]/a/span")
    WebElement stations;

    @FindBy(how = How.XPATH, using = "//*[@id=\"technologies\"]/ul/li[6]/span/span/span[1]")
    WebElement robotsLevel;

    @FindBy(how = How.XPATH, using = "//*[@id=\"technologies\"]/ul/li[1]/span/button")
    WebElement robotsImproveButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"technologies\"]/ul/li[2]/span/span/span[1]")
    WebElement shipyardLevel;

    @FindBy(how = How.XPATH, using = "//*[@id=\"technologies\"]/ul/li[2]/span/button")
    WebElement shipyardImproveButton;

    public WebElement robotsImproveButton() {
        return robotsImproveButton;
    }

    public WebElement robotsLevel() {
        return robotsLevel;
    }

    public WebElement stationsButton() {
        return stations;
    }

    public WebElement shipyardLevel() {
        return shipyardLevel;
    }

    public WebElement shipyardImproveButton() {
        return shipyardImproveButton;
    }

    public boolean isRobotsImproveButtonExist() {
        return !robotsImproveButton().toString().contains("Proxy element");
    }

    public boolean isShipyardImproveButtonExist() {
        return !shipyardImproveButton.toString().contains("Proxy element");
    }
}