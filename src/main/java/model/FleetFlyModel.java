package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;

import java.util.List;

public class FleetFlyModel extends ModelBase {
    public FleetFlyModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.ID, using = "js_eventDetailsClosed")
    WebElement openAllFleetFlayListButton;

    @FindBy(how = How.XPATH, using = "//tr[contains(@id,'eventRow')]")
    List<WebElement> listOfFleets;

    @FindBy(how = How.XPATH, using = "//*[@id=\"menuTable\"]/li[9]/a/span")
    WebElement fleetsOnPlanetButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"sendall\"]")
    List<WebElement> selectAllFleetsButton;
    //WebElement selectAllFleetsButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"continueToFleet2\"]/span")
    WebElement goButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"position\"]")
    WebElement planetNumberInputBox;

    @FindBy(how = How.XPATH, using = "//*[@id=\"continueToFleet3\"]/span")
    WebElement continueButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"allresources\"]/img")
    WebElement selectAllResoucesButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"expeditiontimeline\"]/span[1]/a")
    WebElement timeOfExpeditionDropdown;

    @FindBy(how = How.XPATH, using = "/html/body/ul[4]/li[12]/a")
    WebElement expeditionTime12hours;

    @FindBy(how = How.XPATH, using = "//*[@id=\"sendFleet\"]/span")
    WebElement sendFleetButton;

    public WebElement openAllFleetFlayListButton() {
        return openAllFleetFlayListButton;
    }

    public List<WebElement> listOfFleets() {
        return listOfFleets;
    }

    public boolean isOpenAllFleetFlayListButtonExist() {
        return openAllFleetFlayListButton().isDisplayed();
    }

    public WebElement getFleetsOnPlanet() {
        return fleetsOnPlanetButton;
    }

    public WebElement getOpenAllFleetFlayListButton() {
//        return selectAllFleetsButton;
        return selectAllFleetsButton.get(0);
    }

    public boolean isSelectAllFleetsButtonExist() {
//        return selectAllFleetsButton.isDisplayed();
        if (selectAllFleetsButton.size()>0) return true;
        return false;
    }

    public WebElement getGoButton() {
        return goButton;
    }

    public WebElement getPlanetNumberInputBox() {
        return planetNumberInputBox;
    }

    public WebElement getContinueButton() {
        return continueButton;
    }

    public WebElement getSelectAllResoucesButton() {
        return selectAllResoucesButton;
    }

    public WebElement getTimeOfExpeditionDropdown() {
        return timeOfExpeditionDropdown;
    }

    public WebElement getExpeditionTime12hours() {
        return expeditionTime12hours;
    }

    public WebElement getSendFleetButton() {
        return sendFleetButton;
    }
}