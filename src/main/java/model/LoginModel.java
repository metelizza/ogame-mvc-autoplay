package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginModel extends ModelBase {
    public LoginModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/div/div[2]/ul/li[1]/span")
    WebElement gameLogin;

    @FindBy(how = How.NAME, using = "email")
    WebElement inputEmail;

    @FindBy(how = How.NAME, using = "password")
    WebElement inputPass;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/div/div[2]/div/form/p/button[1]")
    WebElement loginSubmitButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/div/div[2]/div[2]/div[2]/a/button/span")
    WebElement playGameButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"accountlist\"]/div/div[1]/div[2]/div/div/div[11]/button")
    WebElement playWorldButton;

    public WebElement gameLoginButton() {
        return gameLogin;
    }

    public WebElement sendKeysFormEmail() {
        return inputEmail;
    }

    public WebElement sendKeysFormPass() {
        return inputPass;
    }

    public WebElement submitButton() {
        return loginSubmitButton;
    }

    public WebElement submitPlayGame() {
        return playGameButton;
    }

    public WebElement submitPlayWorldButton() {
        return playWorldButton;
    }
}
