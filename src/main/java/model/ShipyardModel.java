package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ShipyardModel extends ModelBase {
    public ShipyardModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[2]/div/ul/li[6]/a/span")
    WebElement shipyard;

    @FindBy(how = How.XPATH, using = "//*[@id=\"details202\"]")
    WebElement mt;

    @FindBy(how = How.XPATH, using = "//*[@id=\"number\"]")
    WebElement mtCount;

    @FindBy(how = How.XPATH,using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[1]/form/div/div[2]/div[3]/a/span")
    WebElement mtSubmit;

    public WebElement mtCount() {
        return mtCount;
    }

    public WebElement shipyard() {
        return shipyard;
    }

    public WebElement mt() {
        return mt;
    }

    public WebElement mtSubmitButton() {
        return mtSubmit;
    }

    public boolean isMtButtonExist() {
        return mt().isDisplayed();
    }

    public boolean isSubmitButtonExist() {
        return mtSubmitButton().isDisplayed();
    }
}
