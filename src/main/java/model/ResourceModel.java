package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ResourceModel extends ModelBase {

    public ResourceModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[5]/div[2]/div[3]/div/ul/li[2]/a/span")
    WebElement mines;



    @FindBy(how = How.XPATH, using = "//*[@id=\"resources_metal\"]")
    WebElement metalRaw;

    @FindBy(how = How.XPATH, using = "//*[@id=\"resources_crystal\"]")
    WebElement crystalRaw;

    @FindBy(how = How.XPATH, using = "//*[@id=\"resources_deuterium\"]")
    WebElement deuteriumRaw;

    @FindBy(how = How.XPATH, using = "//*[@id=\"resources_energy\"]")
    WebElement energyRaw;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[1]/div/div/a[1]/img")
    WebElement metalMineImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[4]/div/div/a[1]/img")
    WebElement energyMineImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[2]/div/div/a[1]/img")
    WebElement crystalMineImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[3]/div/div/a[1]/img")
    WebElement deuteriumMineImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[1]/div/div/a[2]/img")
    WebElement metalStorageImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[2]/div/div/a[2]/img")
    WebElement crystalStorageImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[2]/li[3]/div/div/a[2]/img")
    WebElement deuteriumStorageImproveButton;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/ul[1]/li[5]/div/div/a[1]/img")
    WebElement energyTermoMineImproveButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"producers\"]/li[1]/span/span/span[1]")
    WebElement metalMineLevel;



    public WebElement metalMineLevel() {
        return metalMineLevel;
    }

    public WebElement getMetalRaw() {
        return metalRaw;
    }

    public WebElement getCrystalRaw() {
        return crystalRaw;
    }

    public WebElement getDeuteriumRaw() {
        return deuteriumRaw;
    }

    public WebElement getEnergyRaw() {
        return energyRaw;
    }

    public WebElement metalMineImproveButton() {
        return metalMineImproveButton;
    }

    public WebElement energyMineImproveButton() {
        return energyMineImproveButton;
    }

    public WebElement crystalMineImproveButton() {
        return crystalMineImproveButton;
    }

    public WebElement deuteriumMineImproveButton() {
        return deuteriumMineImproveButton;
    }

    public WebElement metalStorageImproveButton() {
        return metalStorageImproveButton;
    }

    public WebElement crystalStorageImproveButton() {
        return crystalStorageImproveButton;
    }

    public WebElement deuteriumStorageImproveButton() {
        return deuteriumStorageImproveButton;
    }

    public WebElement energyTermoMineImproveButton() {
        return energyTermoMineImproveButton;
    }

    public WebElement minesButton() {
        return mines;
    }



    public boolean isEnergyMineImproveButtonExist() {
        return !energyMineImproveButton().toString().contains("Proxy element");
    }

    public boolean isMetalMineImproveButtonExist() {
        return !metalMineImproveButton().toString().contains("Proxy element");
    }

    public boolean isCrystalMineImproveButtonExist() {
        return !crystalMineImproveButton().toString().contains("Proxy element");
    }

    public boolean isDeuteriumMineImproveButtonExist() {
        return !deuteriumMineImproveButton().toString().contains("Proxy element");
    }

    public boolean isMetalStorageImproveButtonExist() {
        return !metalStorageImproveButton().toString().contains("Proxy element");
    }

    public boolean isCrystalStorageImproveButtonExist() {
        return !crystalStorageImproveButton().toString().contains("Proxy element");
    }

    public boolean isDeuteriumStorageImproveButtonExist() {
        return !deuteriumStorageImproveButton().toString().contains("Proxy element");
    }

    public boolean isEnergyTermoMineImproveButtonExist() {
        return !energyTermoMineImproveButton().toString().contains("Proxy element");
    }

    public boolean isConstructionQueueActive() {
        return false;
    }
}