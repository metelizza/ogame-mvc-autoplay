package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AuctioneerModel extends ModelBase {
    public AuctioneerModel(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[2]/div/ul/li[4]/a/span")
    WebElement auctioneerPage;

    @FindBy(how = How.XPATH, using = "//*[@id=\"js_traderAuctioneer\"]")
    WebElement auction;

    @FindBy(how = How.XPATH, using = "//*[@id=\"nextAuction\"]")
    WebElement nextAuctionTimer;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/div[1]/div[2]/p/span/b")
    WebElement finishAuctionTimer;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/div[1]/div[2]/div[3]")
    WebElement currentBet;

    @FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div/div[3]/div[2]/div[4]/div[2]/div[1]/div[2]/a")
    WebElement currentPlayer;

    @FindBy(how = How.CSS, using = ".js_sliderMetalMax")
    WebElement metalBetButton;

    @FindBy(how = How.CLASS_NAME, using = "pay")
    WebElement pushButton;

    public WebElement auctioneerPage() {
        return auctioneerPage;
    }

    public WebElement auction() {
        return auction;
    }

    public WebElement nextAuctionTimer() {
        return nextAuctionTimer;
    }

    public WebElement finishAuctionTimer() {
        return finishAuctionTimer;
    }

    public WebElement currentBet() {
        return currentBet;
    }

    public WebElement currentPlayer() {
        return currentPlayer;
    }

    public WebElement metalBetButton() {
        return metalBetButton;
    }

    public WebElement pushButton() {
        return pushButton;
    }

    public boolean isNextAuctionTimer() {
        return !nextAuctionTimer().toString().contains("Proxy element");
    }

    public boolean isFinishAuctionTimer() {
        return !finishAuctionTimer().toString().contains("Proxy element");
    }

    public boolean isCurrentBetTextExist() {
        return !currentBet().toString().contains("Proxy element");
    }

    public boolean isCurrentPlayerExist() {
        return !currentPlayer().toString().contains("Proxy element");
    }

    public boolean isMetalBetButtonExist() {
        return !metalBetButton().toString().contains("Proxy element");
    }

    public boolean isPushButtonExist() {
        return !pushButton().toString().contains("Proxy element");
    }

}
