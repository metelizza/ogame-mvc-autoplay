package config;

import java.io.File;
import java.util.concurrent.TimeUnit;

import controller.AppController;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class Config {
    protected WebDriver driver;
    private AppController ogame;
    private JavascriptExecutor js;
    private String siteName = "https://ru.ogame.gameforge.com/#login";

    public void setUp() {
        //final String firebugPath = "src/main/java/recource/uBlock0_1.24.3b1.firefox.signed.xpi";
       // FirefoxProfile fp = new FirefoxProfile();
       // fp.addExtension(new File(firebugPath));
        System.setProperty("webdriver.gecko.driver", "src/main/java/recource/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.navigate().to(siteName);
    }

    public void pageClose() {
        driver.quit();
    }

    public AppController ogame() {
        if (ogame == null) {
            ogame = new AppController(driver);
        }
        return ogame;
    }
}
