package view;

import config.Config;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Game extends Config {

    static final Logger logger = LogManager.getRootLogger();

    public void loginInGame() {
        setUp();
        ogame().ogameLogin().clickGameLogin();
        ogame().ogameLogin().writeEmailForm("2dl@tut.by");
        ogame().ogameLogin().writePassForm("111222");
        ogame().ogameLogin().clickSubmitButton();
        ogame().ogameLogin().clickPlayGameButton();
        ogame().ogameLogin().clickPlayWorldButton();
        ogame().ogameLogin().switchAndCloseAnotherTabs();
        Assert.assertEquals("Bellatrix OGame", driver.getTitle());
    }

    public void printResourceValues(String planetName) {
        System.out.println(planetName);
        System.out.print(ogame().resourceController().getMetalValue() + " | ");
        logger.info(planetName + " metal = " + ogame().resourceController().getMetalValue());
        System.out.print(ogame().resourceController().getCrystalValue() + " | ");
        logger.info(planetName + " crystal = " + ogame().resourceController().getCrystalValue());
        System.out.print(ogame().resourceController().getDeuteriumValue() + " | ");
        logger.info(planetName + " deuterium = " + ogame().resourceController().getDeuteriumValue());
        System.out.print(ogame().resourceController().getEnergyValue() + " | ");
        logger.info(planetName + " energy = " + ogame().resourceController().getEnergyValue());
        System.out.print(ogame().resourceController().sumValues());
        System.out.println();
    }

    public void auctioneer() {
        ogame().auctioneerController().auctioneerPageClick();
        ogame().auctioneerController().auctionClick();
        ogame().auctioneerController().printNextAuctionTime();
        //ogame().auctioneerController().printCurrentBet();
        //ogame().auctioneerController().printCurrentPlayer();
        ogame().auctioneerController().iCanBet();
    }

    public void tryStorageImprove(String planetName) {
        ogame().resourceController().clickMines();
        ogame().resourceController().clickMetalStorageUpdate(planetName);
        ogame().resourceController().clickCrystalStorageUpdate(planetName);
        ogame().resourceController().clickDeuteriumStorageUpdate(planetName);
        ogame().resourceController().clickEnergyMineUpdate(planetName);
        ogame().resourceController().clickEnergyTermoMineUpdate(planetName);
        ogame().resourceController().clickMetalMineUpdate(planetName);
        ogame().resourceController().clickCrystalMineUpdate(planetName);
        ogame().resourceController().clickDeuteriumMineUpdate(planetName);
        int metalMineLevel = ogame().resourceController().metalMineLevel();
        ogame().fabriqeController().clickStation();
        ogame().fabriqeController().clickRobotsUpdate(metalMineLevel, planetName);
        ogame().fabriqeController().shipyardUpdate(metalMineLevel, planetName);
    }

    public void buildMt(String planetName) {
        ogame().shipyardController().clickShipyard();
        ogame().shipyardController().mt();
        ogame().shipyardController().setMtCount();
        ogame().shipyardController().submitMt();
        logger.info(planetName + " build MT");
        System.out.println(planetName + " build MT");
    }

    public void fleets() {
        ogame().fleetFlyController().openAll();
        ogame().fleetFlyController().printAllFleets();
        System.out.println();
    }

    public void saveFleets() {
        ogame().planetsController().moonHomeClick();
        ogame().fleetFlyController().fleetsOnPlanetButtonClick();
        ogame().fleetFlyController().trySelectAndSendAllFleets();
    }

    private void run() {
        try {
            loginInGame();
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_MINUS);

            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_MINUS);

            while (true) {
                saveFleets();
//                System.out.println();
//                ogame().planetsController().homeWorldClick();
//                fleets();
//                printResourceValues("home");
//                tryStorageImprove("home");
//                //buildMt("home");
//                System.out.println();
//                ogame().planetsController().colonyBeerClick();
//                printResourceValues("beer");
//                tryStorageImprove("beer");
//                //buildMt("beer");
//                System.out.println();
//                ogame().planetsController().colonyVodkaClick();
//                printResourceValues("vodka");
//                tryStorageImprove("vodka");
//                //buildMt("vodka");
//                //auctioneer();
//                System.out.println();
//                ogame().planetsController().colonyGinClick();
//                printResourceValues("gin");
//                tryStorageImprove("gin");
//                //buildMt("gin");
//                System.out.println();
//                System.out.println();
//                ogame().planetsController().colonyWineClick();
//                printResourceValues("wine");
//                tryStorageImprove("wine");
//                //buildMt("wine");
//                ogame().planetsController().colonySidreClick();
//                printResourceValues("sidre");
//                tryStorageImprove("sidre");
//                //buildMt("wine");
//
//                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pageClose();
        }
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        while (true) {
            (new Game()).run();
            sleep(5000);
        }
    }
}
