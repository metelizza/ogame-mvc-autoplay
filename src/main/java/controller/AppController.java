package controller;

import org.openqa.selenium.WebDriver;

public class AppController {
    private WebDriver driver;
    private LoginController ogameLogin;
    private ResourceController resourceController;
    private PlanetsController planetsController;
    private AuctioneerController auctioneerController;
    private FleetFlyController fleetFlyController;
    private FabriqeController fabriqeController;
    private ShipyardController shipyardController;

    public AppController(WebDriver driver) {
        this.driver = driver;
    }

    public LoginController ogameLogin() {
        if (ogameLogin == null) {
            ogameLogin = new LoginController(driver);
        }
        return ogameLogin;
    }

    public ResourceController resourceController() {
        if (resourceController == null) {
            resourceController = new ResourceController(driver);
        }
        return resourceController;
    }

    public PlanetsController planetsController() {
        if (planetsController == null) {
            planetsController = new PlanetsController(driver);
        }
        return planetsController;
    }

    public AuctioneerController auctioneerController() {
        if (auctioneerController == null) {
            auctioneerController = new AuctioneerController(driver);
        }
        return auctioneerController;
    }

    public FleetFlyController fleetFlyController() {
        if (fleetFlyController == null) {
            fleetFlyController = new FleetFlyController(driver);
        }
        return fleetFlyController;
    }

    public FabriqeController fabriqeController() {
        if (fabriqeController == null) {
            fabriqeController = new FabriqeController(driver);
        }
        return fabriqeController;
    }

    public ShipyardController shipyardController() {
        if (shipyardController == null) {
            shipyardController = new ShipyardController(driver);
        }
        return shipyardController;
    }
}
