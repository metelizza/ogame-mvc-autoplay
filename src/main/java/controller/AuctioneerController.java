package controller;

import model.AuctioneerModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class AuctioneerController extends BaseController {
    private AuctioneerModel auctioneerModel;

    static final Logger logger = LogManager.getRootLogger();

    public AuctioneerController(WebDriver driver) {
        super(driver);
        auctioneerModel = new AuctioneerModel(driver);
    }

    public void auctioneerPageClick() {
        auctioneerModel.auctioneerPage().click();
    }

    public void auctionClick() {
        auctioneerModel.auction().click();
    }

    public void printNextAuctionTime() {
        if (auctioneerModel.isNextAuctionTimer()) {
            System.out.println("AUCTION: Next in " + auctioneerModel.nextAuctionTimer().getText());
            logger.info("AUCTION: Next in " + auctioneerModel.nextAuctionTimer().getText());
        }
    }

    public int currentBet() {
        if (auctioneerModel.isCurrentBetTextExist()) {
            return Integer.parseInt(auctioneerModel.currentBet().getText().replaceAll("[.]", ""));
        }
        return 0;
    }

    public String currentPlayer() {
        if (auctioneerModel.isCurrentPlayerExist()) {
            return auctioneerModel.currentPlayer().getText();
        }
        return null;
    }

    public void iCanBet() {
        String equalsTimer = "ок. 5м";
        boolean isCurrentPlayerNotMe;
        boolean isBetLess50000;
        boolean canIClickBet;
        boolean isTimeNow = ((auctioneerModel.isFinishAuctionTimer() && auctioneerModel.finishAuctionTimer().getText().equals(equalsTimer)));
        boolean canIClickPushButton;

        while (isTimeNow) {
            auctioneerPageClick();
            auctionClick();
            waitTimes(500);
            isCurrentPlayerNotMe = !currentPlayer().equals("brd");
            isBetLess50000 = currentBet() < 50000;
            canIClickBet = auctioneerModel.isMetalBetButtonExist();
            isTimeNow = ((auctioneerModel.isFinishAuctionTimer() && auctioneerModel.finishAuctionTimer().getText().equals(equalsTimer)));
            canIClickPushButton = auctioneerModel.isPushButtonExist();
            if (isCurrentPlayerNotMe && isBetLess50000 && canIClickBet && canIClickPushButton) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", auctioneerModel.metalBetButton());
                auctioneerModel.metalBetButton().click();
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", auctioneerModel.pushButton());
                auctioneerModel.pushButton().click();
                auctioneerModel.pushButton().click();
                System.out.println("AUCTION: PUSH! Current bet = " + currentBet() + " Current player = " + currentPlayer());
                logger.info("AUCTION: PUSH! Current bet = " + currentBet() + " Current player = " + currentPlayer());
            }
        }
    }
}


