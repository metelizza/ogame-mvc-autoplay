package controller;

import model.FabriqeModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class FabriqeController extends BaseController {
    private FabriqeModel fabriqeModel;

    static final Logger logger = LogManager.getRootLogger();

    public FabriqeController(WebDriver driver) {
        super(driver);
        fabriqeModel = new FabriqeModel(driver);
    }

    public void clickRobotsUpdate(Integer metalMineLevel, String planetName) {
        if ((metalMineLevel >= 5) && (robotsLevel() <= 5) && (fabriqeModel.isRobotsImproveButtonExist())) {
            fabriqeModel.robotsImproveButton().click();
            System.out.println("Robots level UP! on planet" + planetName);
            logger.info("Robots level UP! on planet" + planetName);
        }
        if ((metalMineLevel >= 10) && (robotsLevel() <= 7) && (fabriqeModel.isRobotsImproveButtonExist())) {
            fabriqeModel.robotsImproveButton().click();
            System.out.println("Robots level UP! on planet" + planetName);
            logger.info("Robots level UP! on planet" + planetName);
        }

        if ((metalMineLevel >= 15) && (robotsLevel() <= 10) && (fabriqeModel.isRobotsImproveButtonExist())) {
            fabriqeModel.robotsImproveButton().click();
            System.out.println("Robots level UP! on planet" + planetName);
            logger.info("Robots level UP! on planet" + planetName);
        }
    }

    public void shipyardUpdate(Integer metalMineLevel, String planetName) {
        if ((metalMineLevel>=5) && (shipyardLevel()<=1) && (fabriqeModel.isShipyardImproveButtonExist())) {
            fabriqeModel.shipyardImproveButton().click();
            System.out.println("Shipyard level UP! on planet" + planetName);
            logger.info("Shipyard level UP! on planet" + planetName);
        }
        if ((metalMineLevel>=10) && (shipyardLevel()<=5) && (fabriqeModel.isShipyardImproveButtonExist())) {
            fabriqeModel.shipyardImproveButton().click();
            System.out.println("Shipyard level UP! on planet" + planetName);
            logger.info("Shipyard level UP! on planet" + planetName);
        }

        if ((metalMineLevel>=20) && (shipyardLevel()<=8) && (fabriqeModel.isShipyardImproveButtonExist())) {
            fabriqeModel.shipyardImproveButton().click();
            System.out.println("Shipyard level UP! on planet" + planetName);
            logger.info("Shipyard level UP! on planet" + planetName);
        }

    }

    public void clickStation() {
        fabriqeModel.stationsButton().click();
    }

    public int shipyardLevel() {
        return Integer.parseInt(fabriqeModel.shipyardLevel().getText());
    }

    public int robotsLevel() {
        return Integer.parseInt(fabriqeModel.robotsLevel().getText());
    }

}
