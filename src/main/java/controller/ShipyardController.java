package controller;

import model.ShipyardModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ShipyardController extends BaseController {
    private ShipyardModel shipyardModel;

    static final Logger logger = LogManager.getRootLogger();

    public ShipyardController(WebDriver driver) {
        super(driver);
        shipyardModel = new ShipyardModel(driver);
    }

    public void mt() {
        if (shipyardModel.isMtButtonExist()) {
            shipyardModel.mt().click();
        }
    }

    public void clickShipyard() {
        shipyardModel.shipyard().click();
    }

    public void setMtCount() {
        shipyardModel.mtCount().sendKeys("9999");

    }

    public void submitMt() {
        if (shipyardModel.isSubmitButtonExist()) {
            shipyardModel.mtSubmitButton().click();
        }
    }

//    public Integer getMetalValue() {
//        return Integer.parseInt(shipyardModel.getMetalRaw().getText().replaceAll("[.]", ""));
//    }
//    public void clickEnergyTermoMineUpdate(String planetName) {
//        if ((getEnergyValue() <= 0) && resourceModel.isEnergyTermoMineImproveButtonExist()) {
//            resourceModel.energyTermoMineImproveButton().click();
//            System.out.println("Energy Termo UP! on planet" + planetName);
//            logger.info("Energy Termo UP! on planet" + planetName);
//        }
//    }


}
