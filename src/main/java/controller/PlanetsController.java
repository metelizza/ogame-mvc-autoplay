package controller;

import model.PlanetsModel;
import org.openqa.selenium.WebDriver;

public class PlanetsController extends BaseController {
	private PlanetsModel planetsModel;

	public PlanetsController(WebDriver driver) {
		super(driver);
		planetsModel = new PlanetsModel(driver);
	}

	public void homeWorldClick() {
		planetsModel.homeWorldButton().click();
	}

	public void colonyBeerClick() {
		planetsModel.colonyBeerButton().click();
	}

	public void colonyCalvadosClick() {
		planetsModel.colonyCalvadosButton().click();
	}

	public void colonySidreClick() {
		planetsModel.colonySidreButton().click();
	}

	public void colonyVodkaClick() {
		planetsModel.colonyVodkaButton().click();
	}

	public void colonyGinClick() {
		planetsModel.getColonyGin().click();
	}

	public void colonyWineClick() {
		planetsModel.colonyWineButton().click();
	}

	public void moonHomeClick() {planetsModel.getMoonHome().click();}
}
