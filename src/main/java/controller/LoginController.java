package controller;

import model.LoginModel;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class LoginController extends BaseController {
	private LoginModel loginModel;
	
	public LoginController(WebDriver driver) {
		super(driver);
		loginModel = new LoginModel(driver);
	}

	public void clickGameLogin() {
		loginModel.gameLoginButton().click();
	}

	public void writeEmailForm(String email) {
		loginModel.sendKeysFormEmail().sendKeys(email);
	}

	public void writePassForm(String pass) {
		loginModel.sendKeysFormPass().sendKeys(pass);
	}

	public void clickSubmitButton() {
		loginModel.submitButton().click();
	}

	public void clickPlayGameButton() {
		loginModel.submitPlayGame().click();
	}

	public void clickPlayWorldButton() {
		loginModel.submitPlayWorldButton().click();
	}

	public void switchAndCloseAnotherTabs() {
		waitTimes(3000);
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		driver.close();
		driver.switchTo().window(tabs.get(1));
		waitTimes(3000);
	}
}
