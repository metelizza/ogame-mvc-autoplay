package controller;

import org.openqa.selenium.WebDriver;

public class BaseController {
    protected WebDriver driver;

    public BaseController(WebDriver driver) {
        this.driver = driver;
    }

    public void waitTimes(long enterTime) {
        try {
            Thread.sleep(enterTime);
        } catch (InterruptedException e) {
        }
    }

}
