package controller;

import model.FleetFlyModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FleetFlyController extends BaseController {
    private FleetFlyModel fleetFlyModel;

    static final Logger logger = LogManager.getRootLogger();

    public FleetFlyController(WebDriver driver) {
        super(driver);
        fleetFlyModel = new FleetFlyModel(driver);
    }

    public void printAllFleets() {
        System.out.println("Fly count = " + fleetFlyModel.listOfFleets().size());
        logger.info("Fly count = " + fleetFlyModel.listOfFleets().size());
        for (WebElement webelement : fleetFlyModel.listOfFleets()) {

            WebElement mission = webelement.findElement(
                    By.className("missionFleet"));
            WebElement missionImg = mission.findElement(By.className("tooltipHTML"));
            String missionTitle = missionImg.getAttribute("title");
            System.out.println(("MISSION = " + missionTitle + missionTitle.equals("Чужой флот|Атака")));
            System.out.println("FLY: " + webelement.getText());
            logger.info("FLY: " + webelement.getText());
            String missionPlanet = webelement.findElement(By.className("destFleet")).getText();
            System.out.println("PLANET ON FIRE = " + missionPlanet);
            System.out.println("FLY: Arrival time = " + webelement.findElement(
                    By.className("arrivalTime")).getText());

            String countDown = webelement.findElement((By.xpath("//*[@class='countDown hostile textBeefy']"))).getText();
            System.out.println("COUNTDOWN = " + countDown);

        }
    }

    public void openAll() {
        if (fleetFlyModel.isOpenAllFleetFlayListButtonExist()) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fleetFlyModel.openAllFleetFlayListButton());
            fleetFlyModel.openAllFleetFlayListButton().click();
        }
    }

    public void fleetsOnPlanetButtonClick() {
        fleetFlyModel.getFleetsOnPlanet().click();
    }

    public void trySelectAndSendAllFleets() {
        if (fleetFlyModel.isSelectAllFleetsButtonExist()) {
            fleetFlyModel.getOpenAllFleetFlayListButton().click();
            fleetFlyModel.getGoButton().click();
            fleetFlyModel.getPlanetNumberInputBox().clear();
            fleetFlyModel.getPlanetNumberInputBox().sendKeys("16");
            fleetFlyModel.getContinueButton().click();
            fleetFlyModel.getSelectAllResoucesButton().click();
            fleetFlyModel.getTimeOfExpeditionDropdown().click();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            fleetFlyModel.getExpeditionTime12hours().click();
            fleetFlyModel.getSendFleetButton().click();
        }
    }
}

