package controller;

import model.ResourceModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ResourceController extends BaseController {
    private ResourceModel resourceModel;

    static final Logger logger = LogManager.getRootLogger();

    public ResourceController(WebDriver driver) {
        super(driver);
        resourceModel = new ResourceModel(driver);
    }

    public Integer getMetalValue() {
        return Integer.parseInt(resourceModel.getMetalRaw().getText().replaceAll("[.]", ""));
    }

    public Integer getCrystalValue() {
        return Integer.parseInt(resourceModel.getCrystalRaw().getText().replaceAll("[.]", ""));
    }

    public Integer getDeuteriumValue() {
        return Integer.parseInt(resourceModel.getDeuteriumRaw().getText().replaceAll("[.]", ""));
    }

    public Integer getEnergyValue() {
        return Integer.parseInt(resourceModel.getEnergyRaw().getText().replaceAll("[.]", ""));
    }


    public Integer sumValues() {
        return getMetalValue()+getCrystalValue()+getDeuteriumValue();
    }

    public boolean isMetalValueClassColorEqualsOvermark() {
        return ((resourceModel.getMetalRaw().getAttribute("class").equals("overmark") ||
                (resourceModel.getMetalRaw().getAttribute("class").equals("middlemark"))));
    }

    public boolean isCrystalValueClassColorEqualsOvermark() {
        return ((resourceModel.getCrystalRaw().getAttribute("class").equals("overmark") ||
                (resourceModel.getCrystalRaw().getAttribute("class").equals("middlemark"))));
    }

    public boolean isDeuteriumValueClassColorEqulsOvermark() {
        return ((resourceModel.getDeuteriumRaw().getAttribute("class").equals("overmark") ||
                (resourceModel.getDeuteriumRaw().getAttribute("class").equals("middlemark"))));
    }

    public void clickEnergyMineUpdate(String planetName) {
        if ((getEnergyValue() <= 0) && resourceModel.isEnergyMineImproveButtonExist()) {
            resourceModel.energyMineImproveButton().click();
            System.out.println("Energy UP! on planet " + planetName);
            logger.info("Energy UP! on planet " + planetName);
        }
    }

    public void clickMetalMineUpdate(String planetName) {
        if ((getEnergyValue() > 0) && (resourceModel.isMetalMineImproveButtonExist())) {
            resourceModel.metalMineImproveButton().click();
            System.out.println("Metal UP! on planet " + planetName);
            logger.info("Metal UP! on planet " + planetName);
        }

    }

    public void clickCrystalMineUpdate(String planetName) {
        if ((getEnergyValue() > 0) && (resourceModel.isCrystalMineImproveButtonExist())) {
            resourceModel.crystalMineImproveButton().click();
            System.out.println("Crystal UP! on planet " + planetName);
            logger.info("Crystal UP! on planet " + planetName);
        }
    }

    public void clickDeuteriumMineUpdate(String planetName) {
        if ((getEnergyValue() > 0) && (resourceModel.isDeuteriumMineImproveButtonExist())) {
            resourceModel.deuteriumMineImproveButton().click();
            System.out.println("Deuterium UP! on planet " + planetName);
            logger.info("Deuterium UP! on planet " + planetName);
        }
    }

    public void clickMetalStorageUpdate(String planetName) {
        if ((isMetalValueClassColorEqualsOvermark()) && (resourceModel.isMetalStorageImproveButtonExist())) {
            resourceModel.metalStorageImproveButton().click();
            System.out.println("Metal Storage UP! on planet " + planetName);
        }
    }

    public void clickEnergyTermoMineUpdate(String planetName) {
        if ((getEnergyValue() <= 0) && resourceModel.isEnergyTermoMineImproveButtonExist()) {
            resourceModel.energyTermoMineImproveButton().click();
            System.out.println("Energy Termo UP! on planet" + planetName);
            logger.info("Energy Termo UP! on planet" + planetName);
        }
    }


    public void clickCrystalStorageUpdate(String planetName) {
        if ((isCrystalValueClassColorEqualsOvermark()) && (resourceModel.isCrystalStorageImproveButtonExist())) {
            resourceModel.crystalStorageImproveButton().click();
            System.out.println("Crystal Storage UP! on planet " + planetName);
            logger.info("Crystal Storage UP! on planet " + planetName);
        }
    }

    public void clickDeuteriumStorageUpdate(String planetName) {
        if ((isDeuteriumValueClassColorEqulsOvermark()) && (resourceModel.isDeuteriumStorageImproveButtonExist())) {
            resourceModel.deuteriumStorageImproveButton().click();
            System.out.println("Deuterium Storage UP! on planet " + planetName);
            logger.info("Deuterium Storage UP! on planet " + planetName);
        }
    }



    public void clickMines() {
        resourceModel.minesButton().click();
    }



    public int metalMineLevel() {
        return Integer.parseInt(resourceModel.metalMineLevel().getText());
    }


}


//!constructionQueueIsActive)