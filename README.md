# Ogame simple autoplayer

Простой бот для автоматической игры в браузерную ogame.gameforge.com
Автоматическая постройка зданий и контроль над ставками в аукционах.

Использованы средства Selenium WebDriver 3, maven 1.8, Java SE 8.

Спроектировано с использованием паттерна mvc.

Запуск через: view/Game.java